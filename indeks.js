var unirest = require("unirest");

var req = unirest("GET", "https://shazam.p.rapidapi.com/search");

req.query({
	"term": "kiss the rain",
	"locale": "en-US",
	"offset": "0",
	"limit": "5"
});

req.headers({
	"x-rapidapi-key": "4463d99d4bmshcc4997e4cb34ae3p108a86jsnd2f4a5172333",
	"x-rapidapi-host": "shazam.p.rapidapi.com",
	"useQueryString": true
});


req.end(function (res) {
	if (res.error) throw new Error(res.error);

	console.log(res.body);
});