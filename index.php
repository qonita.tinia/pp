<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Creative - Start Bootstrap Theme</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap Icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- SimpleLightbox plugin CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />

    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="#page-top">SM Ent</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto my-2 my-lg-0">
                        <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                        <li class="nav-item"><a class="nav-link" href="#services">Services</a></li>
                        <li class="nav-item"><a class="nav-link" href="#portfolio">Portfolio</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead" id="about">
            <div class="container px-4 px-lg-5 h-100">
                <div class="row gx-4 gx-lg-5 h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-8 align-self-end">
                        <h1 class="text-white font-weight-bold">NCT 2022</h1>
                        <hr class="divider" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <p class="text-white-75 mb-5">Preparing for NCT project on 2022</p>
                        <a class="btn btn-primary btn-xl" href="#services">
                        <p><span id="tanggalwaktu"></span></p>
                        <script>var dt = new Date();document.getElementById("tanggalwaktu").innerHTML = (("0"+dt.getDate()).slice(-2)) +"."+ (("0"+(dt.getMonth()+1)).slice(-2)) +"."+ (dt.getFullYear()) +" "+ (("0"+dt.getHours()).slice(-2)) +":"+ (("0"+dt.getMinutes()).slice(-2));
                        </script></a>
                    </div>
                </div>
            </div>
        </header>
        <!-- About-->
        <br>
        <?php

        $url = file_get_contents('https://api.kawalcorona.com/indonesia');
        $data = json_decode($url, true);
        ?>
        <div class="container">
            <h2 class="text-center mt-0" >Informasi COVID-19 di indonesia</center></h4>
        <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="bg-primary box text-white">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Positif</center></h5>
                            <h2><?php echo $data[0] ['positif'] ?></h2>
                            <h5>Kasus</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-primary box text-white">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Sembuh</center></h5>
                            <h2><?php echo $data[0] ['sembuh'] ?></h2>
                            <h5>Kasus</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-primary box text-white">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Meninggal</center></h5>
                            <h2><?php echo $data[0] ['meninggal'] ?></h2>
                            <h5>Kasus</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Services-->
        <section class="page-section" id="services">
            <div class="container px-4 px-lg-5">
                <h2 class="text-center mt-0">What Do You Get</h2>
                <hr class="divider" />
                <div class="row gx-4 gx-lg-5">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <div class="mb-2"><i class="bi-gem fs-1 text-primary"></i></div>
                            <h3 class="h4 mb-2">Up to Date Schedule</h3>
                            <p class="text-muted mb-0">Update Schedule of NCT 2022 Project</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <div class="mb-2"><i class="bi-laptop fs-1 text-primary"></i></div>
                            <h3 class="h4 mb-2">Up to Date Photos</h3>
                            <p class="text-muted mb-0">Update Photos During NCT 2022 Project</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <div class="mb-2"><i class="bi-globe fs-1 text-primary"></i></div>
                            <h3 class="h4 mb-2">Pre-Order Albums</h3>
                            <p class="text-muted mb-0">Pre-Order Our Precious Albums!</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <div class="mb-2"><i class="bi-heart fs-1 text-primary"></i></div>
                            <h3 class="h4 mb-2">New Friends</h3>
                            <p class="text-muted mb-0">Meet NCT-zen from another region!</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Portfolio-->
        <div id="portfolio">
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/a.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/a.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-name">NCT 2020 Pt 1</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/b.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/b.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-name">NCT 2020</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/c.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/c.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-name">NCT 2020 Pt 2</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/d.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/d.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-name">NCT 127</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/e.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/e.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-name">Way V</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/f.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/f.jpg" alt="..." />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-name">NCT Dream</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Call to action-->
        <section class="page-section bg-dark text-white">
            <div class="container px-4 px-lg-5 text-center">
                <h2 class="mb-4">Click Here To Listen</h2>
                <img src="assets/play.png" id="icon">
            </div>
            <audio id="mySong">
                <source src="assets/song.mp3" type="audio/mp3">
            </audio>
            <script>
                var mySong = document.getElementById("mySong");
                var icon = document.getElementById("icon")

               icon.onclick = function(){
                      mySong.play();
               }
            </script>           
        </section>
        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-lg-8 col-xl-6 text-center">
                        <h2 class="mt-0">Let's Get In Touch!</h2>
                        <hr class="divider" />
                        <p class="text-muted mb-5">Ready to start your journey with us? Sign Up Now!</p>
                    </div>
                </div>
                <div class="row gx-4 gx-lg-5 justify-content-center mb-5">
                    <div class="col-lg-6">
                        <!-- * * * * * * * * * * * * * * *-->
                        <!-- * * SB Forms Contact Form * *-->
                        <!-- * * * * * * * * * * * * * * *-->
                        <!-- This form is pre-integrated with SB Forms.-->
                        <!-- To make this form functional, sign up at-->
                        <!-- https://startbootstrap.com/solution/contact-forms-->
                        <!-- to get an API token!-->
                        <form id="contactForm" data-sb-form-api-token="API_TOKEN">
                            <!-- Name input-->
                            <div class="form-floating mb-3">
                                <input class="form-control" id="name" type="text" placeholder="Enter your name..." data-sb-validations="required" />
                                <label for="name">Full name</label>
                                <div class="invalid-feedback" data-sb-feedback="name:required">A name is required.</div>
                            </div>
                            <!-- Email address input-->
                            <div class="form-floating mb-3">
                                <input class="form-control" id="email" type="email" placeholder="name@example.com" data-sb-validations="required,email" />
                                <label for="email">Email address</label>
                                <div class="invalid-feedback" data-sb-feedback="email:required">An email is required.</div>
                                <div class="invalid-feedback" data-sb-feedback="email:email">Email is not valid.</div>
                            </div>
                            <!-- Phone number input-->
                            <div class="form-floating mb-3">
                                <input class="form-control" id="phone" type="tel" placeholder="(123) 456-7890" data-sb-validations="required" />
                                <label for="phone">Region</label>
                                <div class="invalid-feedback" data-sb-feedback="phone:required">Region is required.</div>
                            </div>
                            <!-- Message input-->
                            <div class="form-floating mb-3">
                                <textarea class="form-control" id="message" type="text" placeholder="Enter your message here..." style="height: 10rem" data-sb-validations="required"></textarea>
                                <label for="message">Message</label>
                                <div class="invalid-feedback" data-sb-feedback="message:required">A message is required.</div>
                            </div>
                            <!-- Submit success message-->
                            <!---->
                            <!-- This is what your users will see when the form-->
                            <!-- has successfully submitted-->
                            <div class="d-none" id="submitSuccessMessage">
                                <div class="text-center mb-3">
                                    <div class="fw-bolder">Form submission successful!</div>
                                    To activate this form, sign up at
                                    <br />
                                    <a href="https://startbootstrap.com/solution/contact-forms">https://startbootstrap.com/solution/contact-forms</a>
                                </div>
                            </div>
                            <!-- Submit error message-->
                            <!---->
                            <!-- This is what your users will see when there is-->
                            <!-- an error submitting the form-->
                            <div class="d-none" id="submitErrorMessage"><div class="text-center text-danger mb-3">Error sending message!</div></div>
                            <!-- Submit Button-->
                            <div class="d-grid"><button class="btn btn-primary btn-xl disabled" id="submitButton" type="submit">Submit</button></div>
                        </form>
                    </div>
                </div>
                <div class="container">
                <h2 class="text-center text-uppercase text-secondary mb-0">Data NCT-zen</center></h2>
                <br>
                <div class="col-md-12">
          <table class="table table-striped">
              <thead class="thead-dark">
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">id</th>
                      <th scope="col">Name</th>
                      <th scope="col">Region</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                        include "koneksi.php"; //memanggil file connect.php
                        $query_mysql = mysqli_query($conn, "select * from tb_nctzen")or die(mysqli_error($conn)); //mengambil isi database
                        $no = 1; 
                        while($data = mysqli_fetch_array($query_mysql)){ //memecah database menjadi field yang ada di dalamnya
                    ?>
                    <tr>
                      <th scope="row"><?php echo $no++ ?></th>
                      <td><?php echo $data['id']?></td>
                      <td><?php echo $data['Name']?></td> <!-- menampilkan isi field -->
                      <td><?php echo $data['Region']?></td> <!-- menampilkan isi field -->
                    </tr>
                    <?php 
                        }
                    ?>
                    </tbody>
                </table>
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-lg-4 text-center mb-5 mb-lg-0">
                        <i class="bi-phone fs-2 mb-3 text-muted"></i>
                        <div>+1 (555) 123-4567</div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="bg-light py-5">
            <div class="container px-4 px-lg-5"><div class="small text-center text-muted">Copyright &copy; 2021 - Company Name</div></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- SimpleLightbox plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
